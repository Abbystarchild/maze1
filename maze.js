const maze = [
    "wwwwwwwwwwwwwwwwwwwww",
    "w   w     w     w w w",
    "w w w www wwwww w w w",
    "w wsw   w     w w   w",
    "w wwwwwww w www w w w",
    "w         w     w w w",
    "w www wwwww wwwww w w",
    "w w   w   w w   w w w",
    "w wwwww w w w w     w",
    "w     w w w w w w www",
    "wwwww w w w w w w wfw",
    "w     w w w   w w w w",
    "w wwwwwww wwwww w w w",
    "w       w       w   w",
    "wwwwwwwwwwwwwwwwwwwww",
];


// const maze = [
//     `wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww`,
//     `ws            w                         w                      w w`,
//     `w    wwwwwww  www w wwwwwwww ww ww wwwww  wwwwwwwwwwwwwwwwwwww w w`,
//     `wwwwwwww           ww     wwwww  w wwww  ww      wwwwwww       w w`,
//     `w          wwwwww     ww        ww       ww wwww         wwwwwww w`,
//     `ww  www wwwww   wwwww   wwwwwwwwwwwwwwww ww   wwwwwwwwwwww       w`,
//     `www w         wwww ww w     ww           wwww              w ww ww`,
//     `w w w wwwwwww         wwwww wwwwwww wwwww    wwwwwwwwwwwwwww ww ww`,
//     `w   w     wwwwww wwwwwwww       ww  ww    ww w                   w`,
//     `wwwwwwwwww    ww w        ww ww w  ww wwwww  wwwwwwwwwwwwwwwwwww w`,
//     `w        wwww ww wwwwwwwwwwwwww w www ww  w ww ww                w`,
//     `wwwwwww          www            w   w www    w w   wwwwwwwwwwwwwww`,
//     `ww       ww w ww    w wwwwwwwwwwwwww      wwwww ww               w`,
//     `ww wwwww ww wwwwwwwww  ww            wwww ww   wwwwwwwwwwwwwwww ww`,
//     `w       w         www   www wwwwwwwwwww       w               w ww`,
//     `w w www wwwwwwww ww   wwwww    ww       wwww wwwwwww wwwwwwww w ww`,
//     `w   w w          w  w w     ww  ww w ww ww           ww  ww     ww`,
//     `w w w   wwwwwwwwww www  ww ww      w  www wwwwww    ww   w  www ww`,
//     `w  wwwww              wwwwww ww  ww              w wwwwwwwww    ww`,
//     `ww    wwww wwwwwwww wwww      www www wwww ww www  ww         wwww`,
//     `ww w         w   ww wwww ww w        ww wwww w          wwwww   ww`,
//     `w  w  wwww   w w ww      ww ww wwww  www        www w w w    w   w`,
//     `w  w  ww ww wwwww wwwww www    wwww  w    wwwwwwwww w ww ww w ww w`,
//     `ww ww w          w      ww           www            www w  w www w`,
//     `ww ww ww wwwwwwwww wwww  w wwwwwwwww wwwwww wwww ww     ww w   www`,
//     `w  w         w   w    ww         www     ww wwwwwww w w wwww w   w`,
//     `w ww wwwwwww www w ww ww wwwwwwwwwwwwwwwwww         w w      www w`,
//     `w ww             ww w w    w w     w       wwwww wwwwwwwwwwwww   w`,
//     `w wwwwwwwwwwwwww wwww www www   ww ww www   ww ww        ww     ww`,
//     `w      ww      w             wwwww wwwww ww ww w  w wwwww   www ww`,
//     `wwwwww  wwwwww wwwwwwwwwwwww w     w      w ww wwww w w   w w   ww`,
//     `w          w w www    ww     w ww ww wwww w ww         ww w www ww`,
//     `w  wwwww              wwww www  w w              w  wwwwwwww    ww`,
//     `w   w w  w w w w wwww ww w www ww ww w    wwwwwwwww w  ww w   w ww`,
//     `ww  w ww wwwww w w  w   ww ww  w     wwww        ww w  w  wwwww ww`,
//     `w                   www        wwwww      wwwww  w              fw`,
//     `wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww`
// ]

// Maze Key: create multiple mazes in an object. Named maze, 
//           the mazes will have the following attributes: check similar games for ideas
//           L: Landing areas,You got sucked down a sandpit!! You landed safely! todo: create custom art
//           C: cave-ins, the cave crumbles around you as the floor collapses! (danger) todo: create custom art impliment health system.
//           w: walls todo: create more than one color for each level to give variety, add fog of war to undiscovered map regions and have zoomed in focused view on the player to allow larger more detailed maps.
//           M: Creatures that can harm you todo: what do they look like?
//           P: pitfalls, The floor is too soft! *Sandpit* You can pass over this spot one time!
//           f: the way up, on top floor the win condition otherwise its a ladder to a higher floor.
//           Alternative win condition: You intentionally start to go deeper into the maze. on Floor 100 there is a puzzle to defeat a boss and you become the master of the Sunken Tower. *add item that permits player to change maze levels at will.* forshadow with a teleporting enemy weilding said item.
//            Player needs to be able to take more than one damage before reset. The top floor is the most difficult maze.