let playerRow = 3
let playerCol = 3
let playerTop = 0;
let playerLeft = 0;
const mazeOutput = document.querySelector("#mazeOutput")
    // const player = document.getElementById('player')
const player = document.createElement('div')
    // player.classList.add('start')

function canWin(playerRow, playerCol) {
    if (maze[playerRow][playerCol] === "f") {
        player.classList.add('win')
        console.log(maze[playerRow][playerCol])
        return true;
    }

}


function canMove(str) {
    let move = str
    if (move === 'Down') {
        if (maze[playerRow + 1][playerCol] === "w" && playerRow < maze.length) {

            return false;
        } else {
            return true;
        }
    }
    if (move === 'Up') {
        if (maze[playerRow - 1][playerCol] === "w" && playerRow > 0) {
            return false;
        } else {
            return true;
        }
    }
    if (move === 'Left') {
        if (maze[playerRow][playerCol - 1] === "w" && playerCol > 0) {
            return false;
        } else {
            return true;
        }
    }
    if (move === 'Right') {
        if (maze[playerRow][playerCol + 1] === "w" && playerCol < maze[playerRow].length) {
            return false;
        } else {
            return true;
        }
    }
    //     let playerStats = {}
    //         // health: 3,
    //         // inventory: "Traveller's pouch: Has a few lumpy hard biscuts, some honey and water",
}
document.body.addEventListener("keydown", event => {
    if (event.key === "ArrowDown") {
        // document.querySelector('#player')
        if (canMove('Down')) {
            player.classList.add('moveDown')
            playerTop += 25;
            player.style.top = playerTop + "px"
            playerRow += 1
            player.classList.remove('moveDown')
            if (canWin(playerRow, playerCol) === true) {
                player.classList.add('win')
            }
        }
    } else if (event.key === "ArrowUp") {
        if (canMove('Up')) {
            player.classList.add('moveUp')
            playerTop -= 25;
            player.style.top = playerTop + "px"
            playerRow -= 1
            player.classList.remove('moveUp')
            if (canWin(playerRow, playerCol) === true) {
                player.classList.add('win')

            }

        }
    } else if (event.key === "ArrowLeft") {
        if (canMove('Left')) {
            player.classList.add('moveLeft')
            playerLeft -= 25;
            player.style.left = playerLeft + "px"
            playerCol -= 1
            player.classList.remove('moveLeft')
            if (canWin(playerRow, playerCol) === true) {

                player.classList.add('win')

            }

        }
    } else if (event.key === "ArrowRight") {
        if (canMove('Right')) {
            player.classList.add('moveRight')
            playerLeft += 25;
            player.style.left = playerLeft + "px"
            playerCol += 1
            player.classList.remove('moveRight')
            if (canWin(playerRow, playerCol) === true) {

                player.classList.add('win')
            }
        }
    }
});
// maze.dataset[colNum][rowNum]
const displayRow = function(rowStr, index) {
    for (let rowNum = 0; rowNum < maze.length; rowNum++) {
        let rowDiv = document.createElement('div')
        rowDiv.classList.add("row")
        mazeOutput.appendChild(rowDiv)
            // displayBlock(rowStr.charAt(rowNum))

        for (let cellNum = 0; cellNum < maze[rowNum].length; cellNum++) {
            let colDiv = document.createElement('div')
            colDiv.classList.add("cell")
            colDiv.dataset.row = rowNum
            colDiv.dataset.column = cellNum
            rowDiv.appendChild(colDiv)
            if (maze[rowNum][cellNum] === "w") {
                colDiv.classList.add('wall')
            } else if (maze[rowNum][cellNum] === " ") {
                colDiv.classList.add('floor')
            }
            if (maze[rowNum][cellNum] === "s") {
                colDiv.classList.add('start')
            } else if (maze[rowNum][cellNum] === "f") {
                colDiv.classList.add('win!')
            }

        }
    }

}


// query selector element.dataset.row = playerRow + 1 and player.dataset.col = playerCol

function startplayer() {
    let start = document.querySelector('.start')
    start.appendChild(player)
    player.classList.add('player')
        // player.dataset.health = playerStats.health
        // player.dataset.inventory = playerStats.inventory
        // let hearts = playerStats.health
        // let createSpan = document.createElement('span')
        // createSpan.setAttribute('id', 'userInterface')
        // let userInterface = document.querySelector('#userInterface')
        // userInterface.addTextNode("Health: " + hearts.toString())

    // console.log(playerStats)
}
displayRow();
startplayer();